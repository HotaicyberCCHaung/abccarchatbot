<!doctype html>
<html>
<head> <meta charset="utf-8"> </head>
<body>
更多應用參考資料:<br>
https://www.elastic.co/guide/en/elasticsearch/reference/current/search-uri-request.html<br>
https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-query-string-query.html<br>
<hr>
<form>
參數q 格式 關鍵字 或特定欄位:關鍵字 (例如 description:toyota) <input name="q" value="<?php echo $_GET['q']; ?>">
<br>( + - = & | > < ! ( ) { } [ ] ^ " ~ * ? : \ / 這些特殊符號需要濾掉)"<br>
多欄位 description:toyota <b>and</b> color:白色 || description:toyota <b>or</b> description:ford
<hr>
起始 from<input name="from" value="<?php echo $_GET['from']; ?>"><br>
筆數 size<input name="size" value="<?php echo $_GET['size']; ?>"><br>
排序 sort 欄位:ase | desc (例如 mileage:asc 里程少的,_score:desc 關聯分數高的) <input name="sort" value="<?php echo $_GET['sort']; ?>"><br>

<input type="submit" value="送出查詢"><br>

<hr>
</form>
</body>
</html>
<?php
// https://www.elastic.co/guide/en/elasticsearch/reference/current/search-uri-request.html
// https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-query-string-query.html

$url = 'http://127.0.0.1:9200';
$param = '/abccar_car/_search?q='.urlencode($_GET['q']).'&from='.urlencode($_GET['from']).'&size='.urlencode($_GET['size']).'&sort='.urlencode($_GET['sort']);
echo 'query string為: '.$param.'<hr>';
$res = file_get_contents($url.$param);
echo 'return json<br><pre>';
echo $res.'<hr>';
$res = json_decode($res);
echo 'return json decode<br><pre>';
var_dump($res);

?>
