<?php
require 'vendor/autoload.php';
use \NlpTools\Tokenizers\WhitespaceTokenizer;
use \NlpTools\Similarity\JaccardIndex;
use \NlpTools\Similarity\CosineSimilarity;
use \NlpTools\Similarity\Simhash;
 
function repstr( $str ){
    return str_replace(array(' ',"\n","\r","\t"),'',$str);
}


$host = 'mssql-dev'; ///etc/freetds/freetds.conf
$user = 'root'; 
$pass = 'abccar111'; 
$dbname = 'abccar'; 
 
try // Connect to server with try/catch error reporting
  {
  $dbh = new PDO('dblib:host='.$host.';dbname='.$dbname, $user, $pass);
  $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
  }
catch(PDOException $e)
  {
  echo "Couldn't connect to $host/$dbname: ".$e->getMessage(); exit;
  }
//echo "Connected to the $host/$dbname server OK:<br>\n";

$tok = new WhitespaceTokenizer();
//$J = new JaccardIndex();
//$cos = new CosineSimilarity();
$simhash = new Simhash(64);


$sqlStr = 'select carid from car where status = 1';
$stmt = $dbh->prepare($sqlStr);
$stmt->execute();
$aData = $stmt->fetchAll(PDO::FETCH_ASSOC);
$total = count($aData);
echo 'total:'.$total."\n";
//分開撈，筆數多array會炸掉
$sqlStr = 'select * from car where carid = ?';
$stmt = $dbh->prepare($sqlStr);
$sqlStr = 'insert into car_simhash_bruce_test (carid,simhash) values (?,?)';
$stmt2 = $dbh->prepare($sqlStr);
foreach( $aData as $k => $v ){
    $stmt->execute(array($v['carid']));
    $aData = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $text = $aData[0]['BrandID'].'-'.$aData[0]['SeriesName'].'-'.$aData[0]['CategoryName'].'-'.$aData[0]['Price'].'-'.$aData[0]['EngineNo'].'-'.$aData[0]['VehicleNo'].'-'.$aData[0]['PlateNo'].'-'.$aData[0]['Mileage'].'-'.$aData[0]['Description'];
    $text = repstr($text);
    $text_tok = $tok->tokenize($text);
    $text_simhash = $simhash->simhash($text_tok);
    //insert car_simhash_bruce_test
    $stmt2->execute(array($v['carid'],$text_simhash));
    echo $k.'/'.$total.':'.$v['carid'].':'.$text_simhash."\n";
}


