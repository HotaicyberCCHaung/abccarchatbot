<?php
//php compare_all.php [carid] [threshold]  ex: php compare_all.php 1003 80 #找出跟carid 1003有80%相似的

require 'vendor/autoload.php';
use \NlpTools\Tokenizers\WhitespaceTokenizer;
use \NlpTools\Similarity\JaccardIndex;
use \NlpTools\Similarity\CosineSimilarity;
use \NlpTools\Similarity\HammingDistance;
use \NlpTools\Similarity\Simhash;

$host = 'mssql-dev'; ///etc/freetds/freetds.conf
$user = 'root'; 
$pass = 'abccar111'; 
$dbname = 'abccar'; 
 
try // Connect to server with try/catch error reporting
  {
  $dbh = new PDO('dblib:host='.$host.';dbname='.$dbname, $user, $pass);
  $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
  }
catch(PDOException $e)
  {
  echo "Couldn't connect to $host/$dbname: ".$e->getMessage(); exit;
  }

$threshold = intval($argv[1]);
if( !$threshold ){
    $threshold = 90;
}

$sqlStr = 'select * from car_simhash_bruce_test order by carid asc';
$stmt = $dbh->prepare($sqlStr);
$stmt->execute();
$aData = $stmt->fetchAll(PDO::FETCH_ASSOC);

$h = new HammingDistance();

$aRes = array();
//compare

$total_car = count($aData);
echo 'total:'.$total_car."\n";
for($i = 0; $i<$total_car; $i++){
    //echo 'carid:'.$aData[$i]['carid']."\n";
    $sim_carid_array = array();
    for($j = ($i+1); $j<$total_car; $j++){
        $score = 100-$h->dist($aData[$i]['simhash'],$aData[$j]['simhash']);
        if( $score >= $threshold ){
            $sim_carid_array[] = $aData[$j]['carid'].'['.$score.']';
        }
    }
    if( count($sim_carid_array) > 0 ){
        echo $aData[$i]['carid'].':'.implode(',',$sim_carid_array)."\n";
        //$aRes[$v['carid']] = $sim_carid_array;
    }
}

/*
foreach( $aData as $k => $v ){
    $sim_carid_array = array();
    foreach( $aData as $k2 => $v2 ){
        if( $v['carid'] == $v2['carid'] ){
            continue;
        }
        $score = 100-$h->dist($v['simhash'],$v2['simhash']);
        if( $score >= $threshold ){
            $sim_carid_array[] = $v2['carid'].'['.$score.']';
        }
    }
    if( count($sim_carid_array) > 0 ){
        echo $v['carid'].':'.implode(',',$sim_carid_array)."\n";
        //$aRes[$v['carid']] = $sim_carid_array;
    }
}
*/

echo "done\n";


?>
