<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require 'vendor/autoload.php';
use \NlpTools\Tokenizers\WhitespaceTokenizer;
use \NlpTools\Similarity\JaccardIndex;
use \NlpTools\Similarity\CosineSimilarity;
use \NlpTools\Similarity\HammingDistance;
use \NlpTools\Similarity\Simhash;
 

$host = 'mssql-dev'; ///etc/freetds/freetds.conf
$user = 'root'; 
$pass = 'abccar111'; 
$dbname = 'abccar'; 
 
try // Connect to server with try/catch error reporting
  {
  $dbh = new PDO('dblib:host='.$host.';dbname='.$dbname, $user, $pass);
  $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
  }
catch(PDOException $e)
  {
  echo "Couldn't connect to $host/$dbname: ".$e->getMessage(); exit;
  }
//echo "Connected to the $host/$dbname server OK:<br>\n";

$car1 = intval($_GET['car1']);
$car2 = intval($_GET['car2']);

if( !$car1 || ! $car2 ){
	echo 'error: car1=&car2='; exit;
}
 
$sqlStr = 'select * from car where carid in (?,?)';
$stmt = $dbh->prepare($sqlStr);
$stmt->execute(array($car1,$car2));
$aData = $stmt->fetchAll(PDO::FETCH_ASSOC);


// BrandID,SeriesName,CategoryName,Price,EngineNo,VehicleNo,PlateNo,Mileage,Description
//var_dump($aData);
$text1 = $aData[0]['BrandID'].'-'.$aData[0]['SeriesName'].'-'.$aData[0]['CategoryName'].'-'.$aData[0]['Price'].'-'.$aData[0]['EngineNo'].'-'.$aData[0]['VehicleNo'].'-'.$aData[0]['PlateNo'].'-'.$aData[0]['Mileage'].'-'.$aData[0]['Description'];
$text2 = $aData[1]['BrandID'].'-'.$aData[1]['SeriesName'].'-'.$aData[1]['CategoryName'].'-'.$aData[1]['Price'].'-'.$aData[1]['EngineNo'].'-'.$aData[1]['VehicleNo'].'-'.$aData[1]['PlateNo'].'-'.$aData[1]['Mileage'].'-'.$aData[1]['Description'];

function repstr( $str ){
    return str_replace(array(' ',"\n","\r","\t"),'',$str);
}

$text1 = repstr($text1);
$text2 = repstr($text2);

//$text2 = $text1;

$tok = new WhitespaceTokenizer();
$J = new JaccardIndex();
$cos = new CosineSimilarity();
$h = new HammingDistance();
$simhash = new Simhash(64);

$tok1 = $tok->tokenize($text1);
$tok2 = $tok->tokenize($text2);

$simhash1 = $simhash->simhash($tok1);
$simhash2 = $simhash->simhash($tok2);

//$res = $J->similarity($tok1,$tok2);
//$res = $cos->similarity($tok1,$tok2);
//$res = $simhash->similarity($tok1,$tok2);
//$res = gmp_hamdist(gmp_init($simhash1),($simhash2));
$res = 100-$h->dist($simhash1,$simhash2);


?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
</head>
<body>
<textarea cols=100 rows=10>
<?php echo $text1; ?>
</textarea>
simhash:<?php echo $simhash1; ?>
<hr>
<textarea cols=100 rows=10>
<?php echo $text2; ?>
</textarea>
simhash:<?php echo $simhash2; ?>
<hr>
相似度:<?php echo $res; ?>%

</body>
</html>
