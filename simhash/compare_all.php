<?php
//php compare_all.php [carid] [threshold]  ex: php compare_all.php 1003 80 #找出跟carid 1003有80%相似的

require 'vendor/autoload.php';
use \NlpTools\Tokenizers\WhitespaceTokenizer;
use \NlpTools\Similarity\JaccardIndex;
use \NlpTools\Similarity\CosineSimilarity;
use \NlpTools\Similarity\HammingDistance;
use \NlpTools\Similarity\Simhash;

$host = 'mssql-dev'; ///etc/freetds/freetds.conf
$user = 'root'; 
$pass = 'abccar111'; 
$dbname = 'abccar'; 
 
try // Connect to server with try/catch error reporting
  {
  $dbh = new PDO('dblib:host='.$host.';dbname='.$dbname, $user, $pass);
  $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
  }
catch(PDOException $e)
  {
  echo "Couldn't connect to $host/$dbname: ".$e->getMessage(); exit;
  }

$car_id = intval($argv[1]);
$threshold = intval($argv[2]);

$sqlStr = 'select * from car_simhash_bruce_test where carid = ? order by carid';
$stmt = $dbh->prepare($sqlStr);
$stmt->execute(array($car_id));
$aData = $stmt->fetchAll(PDO::FETCH_ASSOC);

$car_simhash = $aData[0]['simhash'];

$sqlStr = 'select * from car_simhash_bruce_test where carid != ?';
$stmt = $dbh->prepare($sqlStr);
$stmt->execute(array($car_id));
$aData = $stmt->fetchAll(PDO::FETCH_ASSOC);


$simhash = new Simhash(64);
$h = new HammingDistance();
$sim_max = 0;
$sim_max_carid = 0;

//compare
foreach( $aData as $k => $v ){

    $score = 100-$h->dist($car_simhash,$v['simhash']);
    if( $score > $sim_max ){
        $sim_max_carid = $v['carid'];
        $sim_max = $score;
    }
    if($score >= $threshold){
        echo 'carid:'.$v['carid'].' -> '.($score)."%\n";
    }
}
echo 'Max -> carid:'.$sim_max_carid.','.$sim_max."\n";
echo "done\n";


function dist_hash($h1,$h2){
	$d = 0;
	for ($i=0;$i<strlen($h1);$i++) {
	    if ($h1[$i]!=$h2[$i])
	        $d++;
	}
	return 100*$d/strlen($h1);
}

?>
