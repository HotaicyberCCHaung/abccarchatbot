<?php
include('db.inc.php');
$req = file_get_contents("php://input",'r');
if( !$req ){
    $req = $_POST['json'];
}

$param = (array)json_decode($req);
//var_dump($param);
$BrandName = $param['BrandName'];
//var_dump($BrandName);exit;
$CategoryName = $param['CategoryName'];
//$CCNum = $param['CCNum'];
$CCNum = $param['CarName'];
//$CarYear = $param['CarYear'];
$CarYear = $param['CarYear'][0];
//var_dump($CarYear);exit;
$sqlStr = "
DECLARE    @SearchBrandName		Nvarchar(20),
		   @SearchCategoryName	Nvarchar(20),
		   @CarName				Nvarchar(20),
		   @ManufactureYear		VARCHAR(4),
		   @BRAND				Nvarchar(20),
		   @CARTYPE				Nvarchar(20)

SET @SearchBrandName = ? 
SET	@SearchCategoryName= ?
SET @CarName = ?	
SET @ManufactureYear = ?

DECLARE @RealPriceFirstQuartile FLOAT,
		@RealPriceThirdQuartile FLOAT,
		@WebPriceFirstQuartile FLOAT,
		@WebPriceThirdQuartile FLOAT

select @BRAND=B.Name,@CARTYPE=C.DisPlayName from CarModel (nolock) A 
inner join Brand (nolock) B ON A.BrandID=B.BrandID 
inner join Series (nolock) C ON A.SeriesID=C.ID
left join Category (nolock) D ON A.CategoryID=D.ID
where A.SearchBrandName=@SearchBrandName and A.SeriesName=@SearchCategoryName

IF @CarName=''
BEGIN
	select MIN(FIRSTQUARTILE) AS PRICE_MIN   , MAX(THIRDQUARTILE) AS PRICE_MAX ,'web' as source
	from WebPrice (nolock)
	where BRAND=@BRAND and CARTYPE=@CARTYPE  AND CARYY=@ManufactureYear
    union
    select MIN(FIRSTQUARTILE) AS PRICE_MIN   , MAX(THIRDQUARTILE) AS PRICE_MAX,'real'  as source
	from RealPrice (nolock)
	where BRAND=@BRAND and CARTYPE=@CARTYPE AND CARYY=@ManufactureYear
	END
ELSE 
BEGIN
	select MIN(FIRSTQUARTILE) AS PRICE_MIN   , MAX(THIRDQUARTILE) AS PRICE_MAX,'web' as source
	from WebPrice (nolock)
	where BRAND=@BRAND and CARTYPE=@CARTYPE AND CARNAME=@CarName AND CARYY=@ManufactureYear
    union
    select MIN(FIRSTQUARTILE) AS PRICE_MIN   , MAX(THIRDQUARTILE) AS PRICE_MAX,'real' as source
	from RealPrice (nolock)
	where BRAND=@BRAND and CARTYPE=@CARTYPE AND CARNAME=@CarName AND CARYY=@ManufactureYear
END
";
//$BrandName = 'TOYOTA';$CategoryName = 'COROLLA ALTIS';$CCNum = '';$CarYear = '2015';

$stmt = $dbh->prepare($sqlStr);
$stmt->execute(array($BrandName,$CategoryName,$CCNum,$CarYear));
$aData = $stmt->fetchAll(PDO::FETCH_ASSOC);
//var_dump($aData);exit;

$aRes = array(
    'Result' => array('realprice_min'=>0,'realprice_max'=>0,'webprice_min'=>0,'webprice_max'=>0)
    ,'ResultCode' => '00000'
    ,'ResultMessage' => 'OK'
);

foreach( $aData as $k => $v ){
    $aRes['Result'][$v['source'].'price_min'] = floatval($v['PRICE_MIN']);
    $aRes['Result'][$v['source'].'price_max'] = floatval($v['PRICE_MAX']);
}

header('Content-Type: application/json; charset=utf-8');
//echo json_encode($aRes,JSON_NUMERIC_CHECK);
var_dump(json_encode($aRes,JSON_NUMERIC_CHECK));
//echo json_encode($aRes,JSON_NUMERIC_CHECK);
?>
