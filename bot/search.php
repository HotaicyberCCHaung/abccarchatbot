<?php
include('db.inc.php');
/*
更多應用參考資料:
https://www.elastic.co/guide/en/elasticsearch/reference/current/search-uri-request.html
https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-query-string-query.html


*/

$req = file_get_contents("php://input",'r');
if( !$req ){
    $req = $_POST['json'];
}
$param = (array)json_decode($req);
//var_dump($param);exit;

if( !$param || count($param) == 0 ){
    $aOutput = array('Result'=>array(),'ResultCode'=>'00000','ResultMessage'=>'json格式錯誤');
    header('Content-Type: application/json; charset=utf-8');
    echo json_encode($aOutput);
    exit;
}



foreach( $param as $k => $v ){
    $param[$k] = str_replace(array(' ',"'",'"',':','<','>','*','&','(',')','-','=','+','%','$','#','@','!','^'),'',$v);
}


$debug= false;
if( $param['debug'] ){
    $debug = true;
}

$aQuery = array();
$query  = '';

//var_dump($aQuery);
//var_dump($param);
//exit;
//特殊條件判斷，根據特色標籤
//小車 - 1200CC以下車輛
if( array_search('小車',$param['Feature']) !== false ){
    $param['EngineDisplacement'] = array(0,1200);
}
//妥善率高 - 找出相關車輛 TOYOTA & LEXUS & Nissan & MAZDA & Honda
if( array_search('妥善率高',$param['Feature']) !== false ){
    $param['BrandName'] = array('TOYOTA','LEXUS','NISSAN','MAZDA','HONDA');
}
//安全性高 - 找出相關車輛 VOLV & Audi & Volkswagen
if( array_search('安全性高',$param['Feature']) !== false ){
    $param['BrandName'] = array('VOLVO','AUDI','VOLKSWAGEN','BMW');
}
//油耗少 - 1400CC以下車輛
if( array_search('油耗少',$param['Feature']) !== false ){
    $param['EngineDisplacement'] = array(0,1400);
}
//車齡少 - 5年內
if( array_search('車齡少',$param['Feature']) !== false ){
    $param['ManufactureYear'] = array(date('Y')-5,date('Y'));
}

//var_dump($param);
//echo "aQuery"."===";
//var_dump($aQuery);
//exit;

//品牌
if(isset($param['BrandName'])){
    $aQuery[] = buildQueryES($param['BrandName'],'brandname');
}

//var_dump($param);
//var_dump($aQuery);
//exit;
//車款
if(isset($param['CategoryName'])){
    $aQuery[] = buildQueryES($param['CategoryName'],'seriesname');
}
//車型
if(isset($param['BodyType'])){
    $aQuery[] = buildQueryES($param['BodyType'],'bodytype');
}
//變速系統
if(isset($param['GearType'])){
    $aQuery[] = buildQueryES($param['GearType'],'geartype');
}
//燃油
if(isset($param['GasType'])){
    $aQuery[] = buildQueryES($param['GasType'],'gastype');
}
//車色
if(isset($param['Color'])){
    $aQuery[] = buildQueryES($param['Color'],'color');
}

//var_dump($param);
//var_dump($aQuery);
//exit;

//內裝/外觀
if(isset($param['Addon'])){
    //$aQuery[] = 'description:'.$param['Addon'];
    //尚未替換，先搜description
    $aQuery[] = buildQueryES($param['Addon'],'description');
}
//安全配備
if(isset($param['Security'])){
    //$aQuery[] = 'description:'.$param['Security'];
    //尚未替換，先搜description
    $aQuery[] = buildQueryES($param['Security'],'description');
}
//安全氣囊數,區間
if(isset($param['Airbag'])){
    $aQuery[] = buildQueryES($param['Airbag'],'airbag',1);
}
//特色標籤
if(isset($param['Feature'])){
    //$aQuery[] = 'feature:'.$param['Feature'];
    //尚未替換，先搜description
    $aQuery[] = buildQueryES($param['Feature'],'description');
}
//賞車地址
if(isset($param['Location'])){
    $aQuery[] = buildQueryES($param['Location'],'address');
}
//乘坐人數
if(isset($param['Passenger'])){
    $aQuery[] = buildQueryES($param['Passenger'],'passenger');
}
//排氣量，區間
if(isset($param['EngineDisplacement'])){
    $aQuery[] = buildQueryES($param['EngineDisplacement'],'enginedisplacement',1);
}
//價錢，區間
if(isset($param['Price'])){
	for($i=0;$i<count($param['Price']);$i++){
		if(strlen($param['Price'][$i])<=4){
			if($i==1){
				$param['Price'][1]="100000000";
			}else{
				$param['Price'][$i]="1";
			}
		}else{
			$param['Price'][$i]=substr($param['Price'][$i],0,-4);
		}
	}
    $aQuery[] = buildQueryES($param['Price'],'price',1);
	//var_dump($param['Price']);exit;
	
}

//年份，區間
if(isset($param['ManufactureYear'])){
    //日期要加工一下
    $param['ManufactureYear'][0] = isset($param['ManufactureYear'][0])?$param['ManufactureYear'][0].'-01-01':'1880-01-01';
    $param['ManufactureYear'][1] = isset($param['ManufactureYear'][1])?$param['ManufactureYear'][1].'-12-31':'*';
    $aQuery[] = buildQueryES($param['ManufactureYear'],'manufacturedate',1);
	//var_dump($param['ManufactureYear']);exit;
}
//關鍵字
if(isset($param['KeyWords'])){
    $aQuery[] = buildQueryES($param['KeyWords'],'description');
}

//只搜有上線的
$aQuery[] = 'status:1';

//把空的條件清除
foreach( $aQuery as $k => $v ){
    if( strlen($v) < 1 ){
        unset($aQuery[$k]);
    }
}

//var_dump($aQuery); 
//var_dump($param);
//exit;
//組合es query並抓取資料
$query = implode(' AND ',$aQuery);

$url = 'http://127.0.0.1:9200';
$param = '/abccar_car_bot/_search?q='.urlencode($query).'&size=100&sort='.urlencode('c("_score:desc","modifydate:desc")');
//$param = '/abccar_car_bot/_search?q='.$query.'&size=100&sort=c("_score:desc","modifydate:desc")';
//var_dump($param);exit;
if($debug){
    $debug_param = urldecode($param);
}else{
    $debug_param = '';
}

$es_url = $url.$param;
//var_dump($es_url);
//$testurl = "http://127.0.0.1:9200/abccar_car_bot/_search?q=brandname:BMW+AND+enginedisplacement:2400+AND+status:1";
//http://127.0.0.1:9200/abccar_car_bot/_search?q=brandname:toyota+AND+seriesname:vios+AND+GearType:汽油
//$testurl = urlencode($testurl);
/*
$ans = file_get_contents($es_url);
$ch=curl_init();
curl_setopt($ch,CURLOPT_URL,$testurl);
curl_setopt($ch,CURLOPT_HEADER,0);
curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
$temp=curl_exec($ch); 
*/
//var_dump($temp);exit;
//var_dump($ans);exit; 
$es_res = (array)json_decode(file_get_contents($es_url));
//var_dump($testurl);
//var_dump($es_res);exit;
//var_dump($es_res['hits']); 
$aCar = (array)$es_res['hits'];
//var_dump($aCar['hits']);
$aCarId = array();

if( !$aCar || count($aCar['hits']) == 0 ){ //完全搜不到車輛，直接回傳錯誤
    $aOutput = array('Result'=>array(),'ResultCode'=>'00000','ResultMessage'=>'搜尋不到車輛','debug'=>urldecode($debug_param)); //需再補上錯誤控制
    header('Content-Type: application/json; charset=utf-8');
    echo json_encode($aOutput);
    exit;
}

foreach( $aCar['hits'] as $k => $v ){
    $v = (array)$v;
    //var_dump($v['_id']); //同等carid
    if( $v['_id'] ){
        $aCarId[] = intval($v['_id']);
    }
}
//var_dump($aCarId); exit;
shuffle($aCarId);
$aCarId = array_slice($aCarId,0,10);

//$aOutput = array('Result'=>array(),'ResultCode'=>'00000','ResultMessage'=>'OK','debug'=>$param,'es'=>json_encode($es_res)); //需再補上錯誤控制
$aOutput = array('Result'=>array(),'ResultCode'=>'00000','ResultMessage'=>'OK','debug'=>urldecode($debug_param)); //需再補上錯誤控制
//$aOutput['debug'] = array("es_url"=>$es_url,"CarId"=>$aCarId,"es_res"=>$es_res);

/*
$sqlStr = 'select ';
$sqlStr .= 'Car.CarID,CONCAT(Brand.Name,Brand.Memo) as BrandName,Car.SeriesName,Car.CategoryName,Car.ManufactureDate,Car.Price';
$sqlStr .= ' from Car join Brand on Car.BrandID = Brand.BrandID';
$sqlStr .= ' where Car.CarId in ('.implode(',',$aCarId).')';
$stmt = $dbh->query($sqlStr);
$aData = $stmt->fetchAll(PDO::FETCH_ASSOC);

foreach( $aData as $k => $v ){
    $sqlStr = 'select top 1 Url from CarFile where CarId = '.intval($v['CarID']).' and Status=1 order by Sequence asc';
    $stmt = $dbh->query($sqlStr);
    $aImg = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $aTmp = array();
    $aTmp['title'] = $v['BrandName'].' '.$v['Price'].'萬';
    $aTmp['image_url'] = $aImg[0]['Url'];
    $aTmp['subtitle'] = date('Y',strtotime($v['ManufactureDate'])).' '.$v['SeriesName'].' '.$v['CategoryName'];
    $aTmp['url'] = 'https://www.abccar.com.tw/buy-car/'.$v['CarID'];
    $aOutput['Result'][] = $aTmp;
}
*/
$sqlStr = "EXEC dbo.usp_GetCarList @list = '".implode(',',$aCarId)."'";
//var_dump(sqlStr);exit;
$stmt = $dbh->query($sqlStr);
$aData = $stmt->fetchAll(PDO::FETCH_ASSOC);
/*
foreach( $aData as $k => $v ){
    $aTmp = array();
    $aTmp['title'] = $v['BrandName'].' '.$v['Price'].'萬';
    $aTmp['image_url'] = $v['Url'];
    $aTmp['subtitle'] = date('Y',strtotime($v['ManufactureDate'])).' '.$v['SeriesName'].' '.$v['CategoryName'];
    $aTmp['url'] = 'https://www.abccar.com.tw/car/'.$v['CarID'];
    $aOutput['Result'][] = $aTmp;
}*/
foreach( $aData as $k => $v ){
    $aTmp = '';
    $aTmp -> title = $v['BrandName'].' '.$v['Price'].'萬';
    $aTmp -> image_url = $v['Url'];
    $aTmp -> subtitle = date('Y',strtotime($v['ManufactureDate'])).' '.$v['SeriesName'].' '.$v['CategoryName'];
    $aTmp -> url = 'https://www.abccar.com.tw/car/'.$v['CarID'];
    $aOutput['Result'][] = $aTmp;
}
//打亂順序
shuffle($aOutput['Result']);
//var_dump($aOutput);exit;
header('Content-Type: application/json; charset=utf-8');
echo (json_encode($aOutput));



function buildQueryES($p,$col,$range=0){ //參數,欄位,range=1表示區間
    $res = '';
    if( $range == 1 && is_array($p) && count($p) > 0){ //區間
        $res =  $col.':[';
        $res .= isset($p[0])?$p[0]:'0';
        $res .=' TO ';
        $res .= isset($p[1])?$p[1]:'*';
        $res .=']';
        return $res;
    }
    if( is_array($p) ){ //如果是陣列，複選 (這邊用OR不用AND)
        $aTmp = array();
        foreach( $p as $k => $v ){
            if( strlen($v) > 0 ){
                $aTmp[] = $v;
            }
        }
        if( count($aTmp) > 0 ){
            $res = $col.':('.implode(' OR ',$aTmp).')';
        }
        return $res;
    }
    //以上皆非->單選
    if(strlen($p) > 0){
        return $col.':'.$p;
    }
}

?>
