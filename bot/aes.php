<?php
$key = 'DEFED44480FB4E6CB596DBD705DDA249';
$iv = '1234567890QWERTY';
//$key = substr('DEFED44480FB4E6CB596DBD705DDA249',0,16);
/*
$iv_byte = array(166, 115, 153, 178, 206, 229, 87, 21, 153, 219, 24, 174, 202, 243, 83, 209);
$iv = '';
foreach( $iv_byte as $k => $v ){
    $iv .= chr($v);
}
//$iv = substr($key,0,16);
*/
echo 'key:'.$key."\n";
echo 'iv:'.$iv."\n";
echo 'iv len:'.strlen($iv)."\n";
$data = $key.'|'.date('Y/m/d H:i:s');
echo 'data:'.$data."\n";

$encrypt_data = openssl_encrypt($data, 'AES-256-CBC', $key, OPENSSL_RAW_DATA, $iv);
echo 'encrypt_data:'.$encrypt_data."\n";

$res_data = openssl_decrypt($encrypt_data, 'AES-256-CBC', $key, OPENSSL_RAW_DATA, $iv);
echo 'decrypt_data:'.$res_data."\n";

?>
