<?php
include('db.inc.php');
/*
http://www.aspbc.com/Index/showtech/id/1136.html

CREATE FUNCTION [dbo].[fnGetDistance](@LatBegin REAL, @LngBegin REAL, @LatEnd REAL, @LngEnd REAL) RETURNS FLOAT
  AS
BEGIN
  --距离(千米)
  DECLARE @Distance REAL
  DECLARE @EARTH_RADIUS REAL
  SET @EARTH_RADIUS = 6378.137  
  DECLARE @RadLatBegin REAL,@RadLatEnd REAL,@RadLatDiff REAL,@RadLngDiff REAL
  SET @RadLatBegin = @LatBegin *PI()/180.0  
  SET @RadLatEnd = @LatEnd *PI()/180.0  
  SET @RadLatDiff = @RadLatBegin - @RadLatEnd  
  SET @RadLngDiff = @LngBegin *PI()/180.0 - @LngEnd *PI()/180.0   
  SET @Distance = 2 *ASIN(SQRT(POWER(SIN(@RadLatDiff/2), 2)+COS(@RadLatBegin)*COS(@RadLatEnd)*POWER(SIN(@RadLngDiff/2), 2)))
  SET @Distance = @Distance * @EARTH_RADIUS  
  --SET @Distance = Round(@Distance * 10000) / 10000  
  RETURN @Distance
END

*/
$req = file_get_contents("php://input",'r');
if( !$req ){
    $req = $_POST['json'];
}
$param = (array)json_decode($req);
$lng = $param['Longitude'];
$lat = $param['Latitude'];
//var_dump($param,$_POST);

//$lng = '121.55657958984375'; $lat = '25.057823181152344';

$sqlStr = 'select DISTINCT Member.MemberID,Member.Name as Dname, *,dbo.fnGetDistance('.floatval($lng).','.floatval($lat).',Longitude,Latitude) as distance from Member inner join MemberFile on Member.MemberID = MemberFile.MemberID where dbo.fnGetDistance('.floatval($lng).','.floatval($lat).',Longitude,Latitude) < 10000 and Member.Category=3 and Member.VIPLevel>0 and Member.Status = 1 order by dbo.fnGetDistance('.floatval($lng).','.floatval($lat).',Longitude,Latitude)';
//echo $sqlStr; exit;
$stmt = $dbh->query($sqlStr);
$aData = $stmt->fetchAll(PDO::FETCH_ASSOC);

//var_dump($aData); exit;
$aRes = array(
        'Result' => array()
        ,'ResultCode' => '00000'
        ,'ResultMessage' => 'OK'
);

shuffle($aData);
$aData = array_slice($aData,0,10);

foreach( $aData as $k => $v ){
    $tel = '';
    $v['Mobile'] = preg_replace('/[^0-9]/','',$v['Mobile']);
    $v['Phone'] = preg_replace('/[^0-9]/','',$v['Phone']);
    if( strlen($v['Mobile'])!= 10 ){
        $tel = $v['Phone'];
    }else{
        $tel = $v['Mobile'];
    }
    $tel = preg_replace('/^0/','+886',$tel);;
    $aTmp = array();
    $aTmp['title'] = $v['Dname'];
    $aTmp['image_url'] = $v['Url'];
    $aTmp['subtitle'] = $tel;
    $aTmp['url'] = 'https://www.abccar.com.tw/dealer/'.$v['MemberID'];
    $aRes['Result'][] = $aTmp;
}

header('Content-Type: application/json; charset=utf-8');
echo json_encode($aRes);

?>
